﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScreenManager : MonoBehaviour {


    private Canvas[] UIs_Array;

    // Use this for initialization
    void Start () {

        //Получение всех потомков элемента типа Canvas
        UIs_Array = transform.GetComponentsInChildren<Canvas>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    //Открывает запрашиваемый интерфейс
    public void openUI(string UI_name)
    {

        //Вызов метода закрывающего все интерфейсы
        Closer();


        //Поиск и включение заданного интерфейса в массиве потомков
        foreach(Canvas el in UIs_Array)
        {

            if (el.name == UI_name)
            {

                //Получение элемента Animator включаемого интерфейса
                Animator UIanim_open = el.GetComponent<Animator>();

                //Переключение тригера в полученном Animator
                UIanim_open.SetBool("open", true);

            }

        }

    }


    //Закрывает ВСЕ интерфейсы потомки
    private void Closer()
    {

        foreach (Canvas el in UIs_Array)
        {

            Animator el_anim = el.GetComponent<Animator>();

            el_anim.SetBool("open", false);

        }

    }

}
