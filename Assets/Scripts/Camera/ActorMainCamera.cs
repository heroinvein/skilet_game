﻿using UnityEngine;

public class ActorMainCamera : MonoBehaviour {

    public GameObject Player;

    private Vector3 offset;

    private Vector3 position;

    public float x; // 0
    public float y; // 7
    public float z; // -3.5



    // Use this for initialization
    void Start () {

        position = new Vector3(x, y, z);

	}

    // Update is called once per frame
    void Update()
    {



    }
	
	// Update is called late once per frame
	void LateUpdate () {

        transform.position = Player.transform.position + position;

    }

}
