﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorStats : MonoBehaviour {

    private float stat_health;
    private float stat_hardines;
    private float stat_mutagen;
    private float stat_armor;
    private float stat_rupture;
    private float stat_radiation;
    private float stat_elec;
    private float stat_chemistry;
    private float stat_heat;


    // Use this for initialization
    void Start () {

        stat_health = 100;
        stat_hardines = 100;
        stat_mutagen = 1;
        stat_armor = 100;
        stat_rupture = 100;
        stat_radiation = 100;
        stat_elec = 100;
        stat_chemistry = 100;
        stat_heat = 100;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdStats()
    {

    }

    void OnGUI()
    {

        GUI.Box(new Rect(10, 70, 300, 300), "Actor stats");
        GUI.Label(new Rect(10, 95, 300, 300), "Health: " + stat_health);
        GUI.Label(new Rect(10, 110, 300, 300), "Energy: " + stat_hardines);
        GUI.Label(new Rect(10, 125, 300, 300), "Mutogen: " + stat_mutagen);
        GUI.Label(new Rect(10, 140, 300, 300), "Armor: " + stat_armor);
        GUI.Label(new Rect(10, 155, 300, 300), "Rupture: " + stat_rupture);
        GUI.Label(new Rect(10, 170, 300, 300), "Radiation: " + stat_radiation);
        GUI.Label(new Rect(10, 185, 300, 300), "Electra: " + stat_elec);
        GUI.Label(new Rect(10, 200, 300, 300), "Chemistry: " + stat_chemistry);
        GUI.Label(new Rect(10, 215, 300, 300), "Heat: " + stat_heat);


    }
}
