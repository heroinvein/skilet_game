﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ActorMobileController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

    private Image joystickBG;
    private Vector3 joysticBasePoint;
    [SerializeField]
    private Image joystick;
    private Vector2 inputVector;


    // Use this for initialization
    void Start()
    {

        //получение базы джойстика
        joystickBG = transform.GetChild(0).GetComponent<Image>();

        //запись начального положения джойстика на экране
        joysticBasePoint = joystickBG.transform.position;

        //Поиск и получение дечернего элемента джойстика на панеле джойстика
        foreach (Transform child in transform)
        {

            if (child.name == "joystick") joystick = child.GetComponent<Image>();

        }

    }


    public virtual void OnPointerDown(PointerEventData ped)
    {

        //Перенесение базы джойстика на место касания 
        joystickBG.transform.position = ped.position;

        OnDrag(ped);

    }


    public virtual void OnPointerUp(PointerEventData ped)
    {

        //Возвращение джойстика на начальное положение
        joystickBG.transform.position = joysticBasePoint;

        //Обнуление вектора
        inputVector = Vector2.zero;

        //Возврат джойстика в центр
        joystick.rectTransform.anchoredPosition = Vector2.zero;

    }


    public virtual void OnDrag(PointerEventData ped)
    {

        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(joystickBG.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {

            //Получение координат позиции касания джойстика
            pos.x = (pos.x / joystickBG.rectTransform.sizeDelta.x);
            pos.y = (pos.y / joystickBG.rectTransform.sizeDelta.x);

            //Вычисление вектора
            inputVector = new Vector2(pos.x * 2, pos.y * 2);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;      

            //Вычисление новых координат джойстика
            float x = (inputVector.x * (joystickBG.rectTransform.sizeDelta.x / 2));
            float y = (inputVector.y * (joystickBG.rectTransform.sizeDelta.y / 2));

            //Установка джойстика на новую позицию
            joystick.rectTransform.anchoredPosition = new Vector2(x, y);

        }

    }

    
    //Вычисление векторов. Используется в ActorController
    public float Horizontal()
    {

        if (inputVector.x != 0) return inputVector.x;
        else return Input.GetAxis("Horizontal");

    }

    //Вычисление векторов. Используется в ActorController
    public float Vertical()
    {

        if (inputVector.y != 0) return inputVector.y;
        else return Input.GetAxis("Vertical");

    }
	
}
