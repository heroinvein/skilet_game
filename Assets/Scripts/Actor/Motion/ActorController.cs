﻿using UnityEngine;

public class ActorController : MonoBehaviour {

    //Скорость ходьбы
    public float speedMove;
    //Сила прыжка
    public float jumpPower;
    //Сила гравитации
    public float gravityPower;

    //Сила гравитации действующей на персонажа
    private float gravityForce;
    //Направление движения персонажа
    private Vector3 moveVector;


    //Ссылки
    private CharacterController ch_controller;
    private Animator ch_animator;
    private ActorMobileController mContr;

	// Use this for initialization
	void Start () {

        ch_controller = GetComponent<CharacterController>();
        ch_animator = GetComponent<Animator>();

        mContr = GameObject.FindGameObjectWithTag("Joystick").GetComponent<ActorMobileController>();
        Debug.Assert(mContr, "Not found ActorMobileController!", this);

    }
	
	// Update is called once per frame
	void Update () {

        GamingGravity();
        CharacterMove();
        
    }

    //Метод перемещения персонажа
    private void CharacterMove()
    {


        if (ch_controller.isGrounded)
        {
            ch_animator.ResetTrigger("Jump");
            ch_animator.SetBool("Falling", false);

            moveVector = Vector3.zero;
            moveVector.x = mContr.Horizontal() * speedMove;
            moveVector.z = mContr.Vertical() * speedMove;

            float gip = Mathf.Abs(Mathf.Sqrt(Mathf.Pow(moveVector.x, 2) + Mathf.Pow(moveVector.z, 2)));


            //Анимация персонажа
            if (gip > 7)
            {

                ch_animator.SetBool("WalkSlow", false);
                ch_animator.SetBool("Walk", false);
                ch_animator.SetBool("Run", true);

            }
            else if (gip < 7 && gip > 3)
            {

                ch_animator.SetBool("WalkSlow", false);
                ch_animator.SetBool("Run", false);
                ch_animator.SetBool("Walk", true);

            }
            else if (gip < 3 && gip != 0)
            {

                ch_animator.SetBool("Run", false);
                ch_animator.SetBool("Walk", false);
                ch_animator.SetBool("WalkSlow", true);

            }
            else
            {

                ch_animator.SetBool("Run", false);
                ch_animator.SetBool("Walk", false);
                ch_animator.SetBool("WalkSlow", false);

            }

            //Поворот игрока в сторону направления движения
            if (Vector3.Angle(Vector3.forward, moveVector) > 1f || Vector3.Angle(Vector3.forward, moveVector) == 0)
            {

                Vector3 direct = Vector3.RotateTowards(transform.forward, moveVector, speedMove, 0.0f);
                transform.rotation = Quaternion.LookRotation(direct);

            }

        }
        else
        {

            if(gravityForce < -3f) ch_animator.SetBool("Falling", true);

        }

        moveVector.y = gravityForce;

        //метод передвижения персонажа
        ch_controller.Move(moveVector * Time.deltaTime);
    }

    //Метод гравитации
    private void GamingGravity()
    {

        if (!ch_controller.isGrounded) gravityForce -= gravityPower * Time.deltaTime;
        else gravityForce = 0f;

        if (Input.GetKeyDown(KeyCode.Space) && ch_controller.isGrounded) gravityForce = jumpPower;

    }

}
