﻿using UnityEngine;
using UnityEngine.UI;

public class ActorPanelHealth : MonoBehaviour {

    [SerializeField]
    private int healthBase;
    [SerializeField]
    private int hardinessBase;
    [SerializeField]
    private int healthCounter;
    [SerializeField]
    private int hardinessCounter;
    [SerializeField]
    private Image health;
    [SerializeField]
    private Image hardiness;


    // Use this for initialization
    void Start () {

        for (int i = 0; i <= healthCounter; i++)
        {

            health.rectTransform.position = new Vector2(20f * i, -9f);

            Instantiate(health, transform);

        }

        for (int i = 0; i <= hardinessCounter; i++)
        {

            hardiness.rectTransform.position = new Vector2(20f * i, -27f);

            Instantiate(hardiness, transform);

        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
