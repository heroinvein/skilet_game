﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Inventory
{

    public class Actor : MonoBehaviour
    {

        [SerializeField]
        public int[] onActor;
        private int count_cells_actor;

        private bool initialized;

        //Ссылки
        private InsertThing insertThing;

        void Start()
        {

            initialized = true;

            count_cells_actor = 0;

        }


        void Update()
        {
            if(initialized)
            {

                insertThing = transform.parent.parent.GetComponent<InsertThing>();

                if (insertThing != null)
                {
                    Inserter();
                }

            }
        }


        private void Inserter()
        {

            Transform[] arrTran = GetCellActor();

            for(int i = 0; i < onActor.Length; i++)
            {

                insertThing.Add(arrTran[i], onActor[i]);

            }

            initialized = false;

        }


        private Transform[] GetCellActor()
        {

            Transform[] arrayTran = new Transform[50];

            int count = 0;

            foreach (Transform tr in transform.GetComponentsInChildren<Transform>())
            {

                if(count != 0) arrayTran[count - 1] = tr;

                count++;

            }

            return arrayTran;

        }

    }

}
