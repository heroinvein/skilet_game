﻿using UnityEngine;
using UnityEngine.UI;
using Inventory;
using System;

namespace Inventory
{

    public class Backpack : MonoBehaviour
    {

        public int[] inBackpack;
        private int count_cells_backpack;

        private bool initialized;

        [SerializeField]
        private Image cell;

        //Ссылки
        private InsertThing insertThing;


        void Start()
        {
            //Добавить получение массива игровых предметов рюкзака, inBackpack из сохранения игрока

            initialized = true;

            count_cells_backpack = 0;


        }


        void Update()
        {
            if (initialized)
            {

                insertThing = transform.parent.parent.parent.parent.GetComponent<InsertThing>();

                if (insertThing != null)
                {
                    AddCellsBackpack();

                    initialized = false;
                }
            }
        }


        private void AddCellsBackpack()
        {

            float cell_size_x = cell.rectTransform.rect.width;
            float cell_size_y = cell.rectTransform.rect.height;


            float panel_backpack_size_x = transform.parent.GetComponent<Image>().rectTransform.rect.width;
            float panel_backpack_size_y = transform.parent.GetComponent<Image>().rectTransform.rect.height;


            float quantity_cell_x = Mathf.Round((panel_backpack_size_x / cell_size_x) - 0.5f);


            float quantity_y = Mathf.Round(inBackpack.Length / quantity_cell_x + 0.4f);


            float indent = (panel_backpack_size_x - quantity_cell_x * cell_size_x) / (quantity_cell_x + 1) - 5;


            float yMax = cell_size_y * quantity_y;


            for (int y = 0; y < quantity_y; y++)
            {


                for (int x = 0; x < quantity_cell_x; x++)
                {


                    float pos_x = (cell_size_x / 2 + x * cell_size_x) + indent * (x + 1);
                    float pos_y = -((cell_size_y / 2 + y * cell_size_y) + indent * y);

                    cell.transform.position = new Vector2(pos_x, pos_y);



                    Image new_cell = Instantiate(cell, transform);

                    insertThing.Add(new_cell.gameObject.GetComponent<Transform>(), inBackpack[count_cells_backpack]);

                    count_cells_backpack++;

                }

            }

            ResizeScrollBackpack(yMax);

        }

        private void ResizeScrollBackpack(float size)
        {

            RectTransform resize = (RectTransform)transform;

            resize.sizeDelta = new Vector2(0, size);

        }

    }

}
