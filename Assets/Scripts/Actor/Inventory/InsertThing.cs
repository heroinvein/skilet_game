﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Inventory
{

    public class InsertThing : MonoBehaviour
    {

        private Library Lib;

        void Start()
        {

            Lib = new Library();

        }


        internal void Add(Transform tr, int id)
        {

            if (id == 0) return;

            try
            {

                Data dt = Lib.Get(id);

                if (dt.Name != null)
                {

                    GameObject go = Resources.Load(dt.Texture) as GameObject;

                    GameObject newThing = Instantiate(go, tr);

                    newThing.name = id.ToString();


                }

            }
            catch (Exception e)
            {

                print("В масииве предметов игрока находится Id не соответствующее ни одному игровому предмету! (Value: " + id + ")");
                print(e);

            }

        }
    }
}