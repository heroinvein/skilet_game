﻿using System.IO;
using UnityEngine;
using Newtonsoft.Json;

namespace Inventory
{

    public class Library
    {

        private DownloadLibrary lib;

        public Library()
        {

            lib = new DownloadLibrary();


        }

        public Data Get(int id)
        {

            foreach(Data dt in lib.Things.Armor)
            {

                if(dt.Id == id)
                {

                    return dt;

                }
            }

            return new Data();

        }

    }

    class DownloadLibrary
    {

        public Container Things;

        public DownloadLibrary()
        {

            Things = Parser();

        }

        private Container Parser()
        {
            string json;

            using (StreamReader r = new StreamReader("Assets/Data/Inventory/Things.json"))
            {
                json = r.ReadToEnd();

                Container data = JsonConvert.DeserializeObject<Container>(json);

                return data;
            }

        }

    }

    internal class Container
    {
        [JsonProperty("Things")]
        public Data[] Armor { get; set; }
    }

    public class Data
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("GameName")]
        public string GameName { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Texture")]
        public string Texture { get; set; }

        [JsonProperty("Parameters")]
        public string Parameters { get; set; }

    }

}
