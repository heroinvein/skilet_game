﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Inventory;
using System;

namespace Inventory
{

    public class Cells : MonoBehaviour
    {

        Transform tran;
        private float idCell;
        Image img;
        private float idTh;

        bool triger;

        void Start()
        {

            triger = true;

        }


        public void Move(Transform tr)
        {

            if (triger)
            {
                tran = tr;
                OneClick();

            }
            else
            {

                TwoClick(tr);

            }

        }

        private void OneClick()
        {

            Image[] imgs = tran.GetComponentsInChildren<Image>();

            if (imgs.Length > 1)
            {

                img = tran.GetComponentsInChildren<Image>()[1];
                img.color = new Color(img.color.r, img.color.g, img.color.b, 0.5f);

                idTh = Int32.Parse(img.name);

                triger = false;

            }

        }

        private void TwoClick(Transform tr)
        {

            idCell = Int32.Parse(tr.name);

            if (CheckCell())
            {

                Image tmp = Instantiate(img, tr);

                tmp.color = new Color(tmp.color.r, tmp.color.g, tmp.color.b, 1f);

                tmp.name = img.name;

                Destroy(img.gameObject);

                img = null;
                tmp = null;
                triger = true;

            }
            else
            {

                img.color = new Color(img.color.r, img.color.g, img.color.b, 1f);

            }

        }

        private bool CheckCell()
        {
            if (idCell >= 100) return true;
            else
            {
                float res = idTh - idCell * 100;
                if (res > 0 && res < idCell * 100) return true;
            }

            return false;

        }




        //public void Set(Transform tr, float id)
        //{

        //    print("Up  : " + id);

        //    tran = tr;


        //    Image[] imgs = tr.GetComponentsInChildren<Image>();

        //    if (imgs.Length > 1)
        //    {
        //        img = tr.GetComponentsInChildren<Image>()[1];

        //        img.rectTransform.sizeDelta += zoom;

        //        print("parent 2: " + transform.parent.parent.parent.name);

        //        Image tmp = Instantiate(img, transform.parent.parent.parent);

        //        Destroy(img);

        //        img = tmp;

        //    }


        //}

        //public void Walk(Vector3 ped, float id)
        //{

        //    print("Drag: " + id);

        //    if (img != null) img.transform.position += ped;


        //}

        //public void Down(GameObject go, float id)
        //{

        //    Transform tr = go.GetComponent<Transform>();



        //    print("Down: " + id);

        //    print("new tran: " + tr.name);



        //    if (img != null)
        //    {

        //        img.rectTransform.sizeDelta -= zoom;

        //        img.transform.position = tr.transform.position;

        //        Instantiate(img, tr);

        //        Destroy(img);

        //        img = null;
        //    }


        //}


    }
}
