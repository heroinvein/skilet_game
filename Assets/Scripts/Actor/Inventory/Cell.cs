﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Inventory
{

    public class Cell : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
    {

        Cells cls;

        private float id;

        public int _id;

        // Use this for initialization
        void Start()
        {

            cls = transform.root.GetComponent<Cells>();

            if(_id > 0)
            {
                id = _id;
            }
            else
            {
                id = Mathf.Round(Random.Range(100f, 999f));
            }
            

            transform.name = id.ToString();

        }


        public virtual void OnPointerDown(PointerEventData ped)
        {

            cls.Move(transform);

        }


        public virtual void OnPointerUp(PointerEventData ped)
        {


        }

        public virtual void OnDrag(PointerEventData ped)
        {


        }

    }
}
