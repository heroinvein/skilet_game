﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RulesUI : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{

    //Имя корневого родителя
    [SerializeField]
    private string UI_main;

    //Имя интерфейса на который необходимо переключиться
    [SerializeField]
    private string UI_name;

    //Ссылка на менеджер интерфейсов их переключающих, находится в корневом родителе елементов
    private ScreenManager mScreen;

    // Use this for initialization
    void Start () {

        GetScreenManager();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void OnPointerDown(PointerEventData ped)
    {



    }

    public virtual void OnPointerUp(PointerEventData ped)
    {

        //Переключаем интерфейс вызовом метода менеджера
        mScreen.openUI(UI_name);

    }


    //Получение ссылки на менеджер интерфейсов корневого родителя элемента
    public void GetScreenManager()
    {

        Transform el = transform.parent;

        while(true)
        {

            if (el.name == UI_main)
            {

                mScreen = el.GetComponent<ScreenManager>();
                break;

            }
            else
            {

                el = el.parent;

            }

        }

    }
}
