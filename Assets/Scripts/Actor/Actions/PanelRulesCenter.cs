﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PanelRulesCenter : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{

    [SerializeField]
    private Image button;

    private Vector2 zero;


    // Use this for initialization
    void Start () {

        zero = button.rectTransform.sizeDelta;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void OnPointerDown(PointerEventData ped)
    {

        button.rectTransform.sizeDelta = new Vector2(zero.x * 1.2f, zero.y * 1.2f);

    }

    public virtual void OnPointerUp(PointerEventData ped)
    {

        button.rectTransform.sizeDelta = zero;

    }

}
